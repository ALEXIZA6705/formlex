<?php
      class Usuario extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
        }

        public function index(){
          $data["listado"]=$this->usuarios->consultarTodos();
          $this->load->view("header");
          $this->load->view("usuarios/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("usuario/nuevo");
          $this->load->view("footer");
        }

        public function guardarUsuario(){
            $datosNuevoUsuario=array(

                "apellido_usu"=>$this->input->post("apellido_usu"),
                "nombre_usu"=>$this->input->post("nombre_usu"),
                "telefono_usu"=>$this->input->post("telefono_usu"),
                "direccion_usu"=>$this->input->post("direccion_usu"),
                "email_usu"=>$this->input->post("email_usu"),

            );
            if($this->usuario->insertar($datosNuevoUsuario)){
                echo "INSERCION EXITOSA";
            }else{
                echo "ERROR AL INSERTAR";
            }
        }



    }//cierre de la clase
?>
