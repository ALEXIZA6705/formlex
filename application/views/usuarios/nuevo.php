<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/usuarios/guardarUsuario"method="post">
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"  type="text" name="apellido_usu" id="apellido_usu" placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_usu" id="nombre_usu" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control"  type="number" name="telefono_usu" id="telefono_usu" placeholder="Por favor Ingrese el telefono">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control"  type="text" name="direccion_usu" id="direccion_usu" placeholder="Por favor Ingrese la dirección">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control"  type="email" name="email_usu" id="email_usu" placeholder="Por favor Ingrese el correo">
    <br>
    <br>

    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/usuarios/index"
      class="btn btn-warning">
      CANCELAR
    </a>

</form>
</div>
</div>
</div>
