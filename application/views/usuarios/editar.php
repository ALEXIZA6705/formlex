<h1>ACTUALIZACION DE USUARIOS</h1>
<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/usuarios/procesarActualizacion"
  method="post"
  >
      <input class="form-control"  type="hidden" name="id_usu" id="id_usu"
       value="<?php echo $usuario->id_usu; ?>">
    <br>
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"  type="text" name="apellido_usu" id="apellido_usu" placeholder="Por favor Ingrese el apellido"
     value="<?php echo $usuario->apellido_usu; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_usu" id="nombre_usu" placeholder="Por favor Ingrese el nombre"
     value="<?php echo $usuario->nombre_usu; ?>">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control"  type="number" name="telefono_usu" id="telefono_usu" placeholder="Por favor Ingrese el telefono"
     value="<?php echo $usuario->telefono_usu; ?>">
    <br>
    <br>
    <label for="">DIRECCIÓN</label>
    <input class="form-control"  type="text" name="direccion_usu" id="direccion_usu" placeholder="Por favor Ingrese la dirección"
     value="<?php echo $usuario->direccion_usu; ?>">
    <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control"  type="email" name="email_usu" id="email_usu" placeholder="Por favor Ingrese el correo"
     value="<?php echo $usuario->email_usu; ?>">
    <br>
    <br>

    <button type="submit" name="button">
      ACTUALIZAR
    </button>
    <a href="<?php echo site_url(); ?>/usuarios/index">
      Cancelar
    </a>
</form>
</div>
</div>
</div>
